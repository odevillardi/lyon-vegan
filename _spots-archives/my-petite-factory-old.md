---
layout: spot
title: "My petite factory"
favori: oui
type-class: restaurant
vegan: Options véganes
code-postal: "69007"
img: /dist/img/spots/my-petite-factory.jpg
related:
  - spot1: null
    url: /spots/hank-burger.html
    nom: Hank burger
horaires:
  - lundi: 11h30 - 14h
  - mardi: Fermé
  - mercredi: 11h30 - 14h
  - jeudi: 11h30 - 14h
  - vendredi: 11h30 - 14h
  - Samedi: 11h30 - 14h
  - dimanche: Fermé
liens:
  - facebook: null
    nom: Facebook
    linkurl: https://www.facebook.com/mypetitefactory/
  - Instagram: null
    nom: Instagram
    linkurl: https://www.instagram.com/mypetitefactory
  - site: null
    nom: Site Internet
    linkurl: https://mypetitefactory.com
class: spot-title
prix: €€
emporter: Plats à emporter
adresse: 26 rue de la Thibaudière
ville: Lyon
img-logo: /dist/img/spots/my-petite-factory-logo.jpg
title: My petite factory
menu: spots
description: On n’a pas encore testé *My petite factory*, ma la carte et la
  philosophie nous plaisent beaucoup.
type: Café – Restaurant – Traiteur
tel: 04 87 24 18 43
alt-img: Intérieur de my petite factory
alt-img-logo: Logo de my petite factory
spotid: my-petite-factory
tel-int: "+33487241843"
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=4.844136536121369%2C45.74948142414001%2C4.845866560935975%2C45.75087764806571&amp;layer=mapnik&amp;marker=45.75017954046873%2C4.845001548528671"></iframe>

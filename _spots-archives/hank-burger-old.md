---
layout: spot
title: "Hank Burger"
favori: oui
type-class: restaurant
vegan: 100% Végan
code-postal: "69001"
img: /dist/img/spots/hank-burger.jpg
related:
  - spot1: null
    url: /spots/my-petite-factory.html
    nom: My petite factory
horaires:
  - lundi: 11h30 - 14h / 18h - 21h
  - mardi: 11h30 - 14h / 18h - 21h
  - mercredi: 11h30 - 14h / 18h - 21h
  - jeudi: 11h30 - 14h / 18h - 21h
  - vendredi: 11h30 - 14h / 18h - 21h
  - Samedi: 11h30 - 21h
  - dimanche: 11h30 - 14h / 18h - 21h
class: spot-title
prix: €€
emporter: Plats et menus à emporter
adresse: 5 rue Pizay
ville: Lyon
liens:
  - facebook: null
    nom: Facebook
    linkurl: https://www.facebook.com/hankrestaurant/
  - Instagram: null
    nom: Instagram
    linkurl: https://www.instagram.com/hankrestaurant/
  - site: null
    nom: Site Internet
    linkurl: https://www.hankrestaurant.com
img-logo: /dist/img/spots/hank-burger-logo.jpg
title: Hank Burger
menu: spots
description: Chez *Hank*, les burgers **végans** sont majestueux, les sides
  savoureux et les desserts gourmands.
type: Fast food vegan
tel: 04 28 29 79 12
alt-img: Devanture de Hank Burger à Lyon
alt-img-logo: Devanture de Hank Burger à Lyon
spotid: hank-burger
tel-int: "+33428297912"
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=4.834284782409669%2C45.76622632177541%2C4.836014807224275%2C45.767622126767584&amp;layer=mapnik&amp;marker=45.76692422863731%2C4.835149794816971"></iframe>

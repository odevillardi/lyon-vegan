---
layout: spot
menu: spots
class: spot-title
title: Ninkasi
description: Chez *Ninkasi*, pas mal d’**options végétales**, entre le burger et les starters, de quoi se faire plaisir.
favori: oui
type: Brasserie
type-class: restaurant
vegan: Options véganes
prix: €€
emporter: Click & Collect + Livraison
adresse: 267 Rue Marcel Mérieux
code-postal: "69007"
ville: Lyon
tel: 04 72 76 52 34
tel-int: "+33472765234"
liens:
  - facebook: null
    nom: Facebook
    linkurl: https://www.facebook.com/Ninkasifr/
  - Instagram: null
    nom: Instagram
    linkurl: https://www.instagram.com/ninkasifr/
  - site: null
    nom: Site Web
    linkurl: https://www.ninkasi.fr
img: /dist/img/spots/ninkasi.jpg
alt-img: Ninkasi
img-logo: /dist/img/spots/ninkasi-logo.jpg
alt-img-logo: Logo Ninkasi
horaires:
  lundi: 10h45 - 01h
  mardi: 10h45 - 01h
  mercredi: 10h45 - 01h
  jeudi: 10h45 - 01h
  vendredi: 10h45 - 04h
  samedi: 10h45 - 04h
  dimanche: 10h45 - 00h
related:
  - url: copper-branch.html
    nom: Copper Branch
  - url: hank-burger.html
    nom: Hank Burger
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=4.8252296447753915%2C45.72544630074825%2C4.835937023162843%2C45.73062149356422&amp;layer=mapnik&amp;marker=45.728035829447236%2C4.830577969551086"></iframe>

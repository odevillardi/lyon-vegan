---
layout: spot
menu: spots
class: spot-title
title: Au Bonheur des Chats
description: "*Au Bonheur des Chats* est un bar à chat, ou neko café, un peu
  **atypique**. Chats à l'adoption, **boissons et nourriture végétaliennes**
  sont à l'honneur dans une ambiance relaxante et chaleureuse."
favori: non
type: Neko café
type-class: cafe
vegan: 100% Végan
prix: €€
emporter: Réservation fortement conseillée
adresse: 5 rue de Bonald
code-postal: "69007"
ville: Lyon
tel: 09 87 18 24 24
tel-int: "+33987182424"
liens:
  - nom: Site Web
    linkurl: https://www.au-bonheurdeschats.fr/
img: https://image.jimcdn.com/app/cms/image/transf/none/path/scce8948fea247cdc/backgroundarea/ia62066a55733c5bb/version/1631011246/image.jpg
alt-img: Intérieur du café "Au bonheur des chats"
img-logo: https://image.jimcdn.com/app/cms/image/transf/dimension=94x10000:format=png/path/scce8948fea247cdc/image/idc781bcb24d09ec4/version/1565081346/image.png
alt-img-logo: Logo du café "Au bonheur des chats"
horaires:
  lundi: Fermé
  mardi: Fermé
  mercredi: 10h30 - 18h30
  jeudi: 10h30 - 18h30
  vendredi: 10h30 - 18h30
  samedi: 11h30 - 18h30
  dimanche: 11h30 - 18h30
related:
  - url: zoi.html
    nom: Zoï
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=4.8374390602111825%2C45.75214470040576%2C4.841161966323853%2C45.754712405114915&amp;layer=mapnik&amp;marker=45.753428567527614%2C4.839300513267517"></iframe>

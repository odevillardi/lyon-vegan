---
layout: spot
menu: spots
class: spot-title
title: Copper Branch
description: "*Copper Branch* est une enseigne canadienne proposant un menu
  **100% végétal**. Notre mission est de vous servir des **recettes simples**,
  **équilibrées et gourmandes** à un prix raisonnable dans une ambiance
  décontractée."
favori: non
type: Fast Food Vegan
type-class: restaurant
vegan: 100% Végan
prix: €€
emporter: Plats à emporter
adresse: 112, Cours Charlemagne
code-postal: "69002"
ville: Lyon
tel: 06 72 89 96 77
tel-int: "+33672899677"
liens:
  - nom: Facebook
    linkurl: https://www.facebook.com/copperbranchlyon/
  - nom: Site Web
    linkurl: https://copperbranch.fr
img: /dist/img/spots/copper-branch.jpg
alt-img: Devanture de Copper Branch
img-logo: /dist/img/spots/copper-branch-logo.png
alt-img-logo: Logo de Copper Branch
horaires:
  lundi: 11h30 - 21h30
  mardi: 11h30 - 21h30
  mercredi: 11h30 - 21h30
  jeudi: 11h30 - 21h30
  vendredi: 11h30 - 21h30
  samedi: 11h30 - 21h30
  dimanche: 11h30 - 21h30
related:
  - nom: Hank Burger
    url: hank-burger.html
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=4.8135244846344%2C45.738080117430734%2C4.8241567611694345%2C45.74325413966156&amp;layer=mapnik&amp;marker=45.74066999637929%2C4.818844646215439"></iframe>

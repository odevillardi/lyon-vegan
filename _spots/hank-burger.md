---
layout: spot
menu: spots
class: spot-title
title: Hank Burger
description: Chez *Hank*, les burgers **végans** sont majestueux, les sides
  savoureux et les desserts gourmands.
favori: oui
type: Fast food vegan
type-class: restaurant
vegan: 100% Végan
prix: €€
emporter: Plats à emporter
adresse: 5 rue Pizay
code-postal: "69001"
ville: Lyon
tel: 04 28 29 79 12
tel-int: "+33428297912"
liens:
  - facebook: null
    nom: Facebook
    linkurl: https://www.facebook.com/hankrestaurant/
  - Instagram: null
    nom: Instagram
    linkurl: https://www.instagram.com/hankrestaurant/
  - site: null
    nom: Site Web
    linkurl: https://www.hankrestaurant.com
img: /dist/img/spots/hank-burger.jpg
alt-img: Hank burger
img-logo: /dist/img/spots/hank-burger-logo.jpg
alt-img-logo: Logo Hank Burger
horaires:
  lundi: 11h30 - 14h / 18h - 21h
  mardi: 11h30 - 14h / 18h - 21h
  mercredi: 11h30 - 14h / 18h - 21h
  jeudi: 11h30 - 14h / 18h - 21h
  vendredi: 11h30 - 14h / 18h - 21h
  samedi: 11h30 - 21h
  dimanche: 11h30 - 14h / 18h - 21h
related:
  - url: copper-branch.html
    nom: Copper Branch
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=4.834284782409669%2C45.76622632177541%2C4.836014807224275%2C45.767622126767584&amp;layer=mapnik&amp;marker=45.76692422863731%2C4.835149794816971"></iframe>

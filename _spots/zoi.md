---
layout: spot
menu: spots
class: spot-title
title: Zoï
description: Chez *Zoï*, vous trouverez des **pâtisseries végétales** qui vous
  feront oublier que vous venez de gravir la montée de la Grande-Côte...
favori: non
type: Patisserie
type-class: cafe
vegan: 100% Végan
prix: €
emporter: oui
adresse: 46 Montée de la Grande-Côte
code-postal: "69003"
ville: Lyon
tel: 06 02 16 76 01
tel-int: "+330602167601"
liens:
  - nom: Site Web
    linkurl: https://www.zoi-kitchen.com
img: /dist/img/spots/zoi.jpg
alt-img: Vitrine Zoï
img-logo: /dist/img/spots/zoi-logo.jpg
alt-img-logo: Logo Zoï
horaires:
  lundi: Fermé
  mardi: Ouvert
  mercredi: Ouvert
  jeudi: Ouvert
  vendredi: Ouvert
  samedi: Ouvert
  dimanche: Fermé
related:
  - url: au-bonheur-des-chats.html
    nom: Au bonheur des chats
---
<iframe src="https://www.openstreetmap.org/export/embed.html?bbox=4.828888177871705%2C45.770198474007046%2C4.834145307540894%2C45.772522136112705&amp;layer=mapnik" style="border: 1px solid black"></iframe>
